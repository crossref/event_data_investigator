# Event Data Reports
# Production build of Crossref Event Data Investigator

FROM clojure:lein-2.7.0-alpine

COPY target/uberjar/*-standalone.jar ./investigator.jar

ENTRYPOINT ["java", "-jar", "investigator.jar"]
